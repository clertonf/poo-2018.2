
package br.ufc.quixada.es.poo.exec;

import br.ufc.quixada.es.poo.model.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class PrincipalIprimivel {
    public static void main(String[] args) {

        Cliente cliente1 = new ClientePessoaFísica("Clerton Filho", "Rua Agamenon", "1233123-12", LocalDate.of(1999, 02, 21));
        Cliente cliente2 = new ClientePessoaFísica("Gilvan Oliveira", "Rua Luzia de Pinho", "3261233-67", LocalDate.of(1999, 5, 03));
        Cliente cliente3 = new ClientePessoaJurídica("Fernanda Nascimento", "Rua General Sampaio", "82192819-2435.4", LocalDate.of(1990, 2, 13));

        Vendedor vendedor1 = new Vendedor("Jovirone Xesque", "219218-4", "2345", 1500,8);
        Vendedor vendedor2 = new Vendedor("Jukes Mec", "94812-5", "43132", 2000,7);
        Vendedor vendedor3 = new Vendedor("Yoda Fon", "3145536-5", "3434", 1500,10);
        
         Passagem passagem1 = new Passagem(023, cliente1, vendedor1, LocalDate.of(2018, 2, 30), 02,02,24);
        Passagem passagem2 = new Passagem(012, cliente2, vendedor2, LocalDate.of(2018, 7, 21), 14,23,300);
        Passagem passagem3 = new Passagem(3, cliente3, vendedor3, LocalDate.of(2018, 10, 18),10,32,202);

        List<Passagem> passagens = new ArrayList<>();
        passagens.add(passagem1);
        passagens.add(passagem2);
        passagens.add(passagem3);

        List<Cliente> clientes = new ArrayList<>();
        clientes.add(cliente1);
        clientes.add(cliente2);
        clientes.add(cliente3);
        
        List<Vendedor> vendedores = new ArrayList<>();
        vendedores.add(vendedor1);
        vendedores.add(vendedor2);
        vendedores.add(vendedor3);


      Imprimivel imprimivel1 = (72,"Quixadá",passagens, clientes);
        
        imprimivel1.mostrarPassagens();
       
    }


}
