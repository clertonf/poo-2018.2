

package br.ufc.quixada.es.poo.model;


public abstract class ServiçosGerais extends Funcionário {
    private int tempoServiço;

    public void ServicosGerais() {
    }

    public ServiçosGerais(String nome, String cpf, String matricula, float salario, int tempoServiço) {
        super(nome, cpf, matricula, salario);
        this.tempoServiço = tempoServiço;
    }
       @Override
    public void darBonificacao() {
        setSalario(getSalario() + 3);
    }

    public void limpar() {
        darBonificacao ();
    }

    @Override
    public String toString() {
        return "ServiçosGerais " +tempoServiço + super.toString();
    }
}
