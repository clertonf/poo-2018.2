package br.ufc.quixada.es.poo.model;

import java.time.LocalDate;

public class Passagem {

    private int  codPassagem;
    private Cliente     cliente;
    private Funcionário vendedor;
    private LocalDate   dataPartida;
    private int horario;
    private int Poltrona;
    private float valorPassagem;

    public void Passagem() {
    }

    public Passagem(int codPassagem, Cliente cliente, Funcionário vendedor, LocalDate dataPartida, int horario, int poltrona, float valorPassagem) {
        this.codPassagem = codPassagem;
        this.cliente = cliente;
        this.vendedor = vendedor;
        this.dataPartida = dataPartida;
        this.horario = horario;
        this.Poltrona = Poltrona;
        this.valorPassagem = valorPassagem;
    }

    public int getCodPassagem() {
        return codPassagem;
    }

    public void setCodPassagem(int codPassagem) {
        this.codPassagem = codPassagem;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Funcionário getVendedor() {
        return vendedor;
    }

    public void setVendedor(Funcionário vendedor) {
        this.vendedor = vendedor;
    }

    public LocalDate getDataPartida() {
        return dataPartida;
    }

    public void setDataPartida(LocalDate dataPartida) {
        this.dataPartida = dataPartida;
    }
    
    public int getHorario(){
        return horario;
    }
    
    public void setHorario(int horario){
        this.horario = horario;
    }
    
    public int getPoltrona(){
        return Poltrona;
    }
    
    public void setPoltrona(int Poltrona){
        this.Poltrona = Poltrona;
    }

    public float getValorPassagem() {
        return valorPassagem;
    }

    public void setValorPassagem(float valorPassagem) {
        this.valorPassagem = valorPassagem;
    }

    @Override
    public String toString() {
        return "Passagem Código da Passagem = " + codPassagem +
                ", cliente = " + cliente + ", vendedor = " + vendedor +
                ", dataPartida = " + dataPartida + ", Horario = " + horario +
                ", Poltrona = " + Poltrona + " Valor da Passagem " + valorPassagem;
    }

   

  
}



