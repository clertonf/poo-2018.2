package br.ufc.quixada.es.poo.model;

 
public abstract class Motorista extends Funcionário {
        private String cnh;

    public Motorista(String nome, String cpf, String matricula, float salario, String cnh) {
        super(nome, cpf, matricula, salario);
        this.cnh = cnh;
    }

    public void Motorista() {
    }
  
     @Override
    public void darBonificacao() {
        setSalario(getSalario() + getSalario() * 0.05f);
    }
    
    public void realizarViagem(){
        darBonificacao();
     }
    
   public String getCnh() {
        return cnh;
    }

    
    public void setCnh(String cnh) {
        this.cnh = cnh;
    }

      @Override
        public String toString() {
        return "Motorista" +cnh + super.toString();
    }
 

    

}
