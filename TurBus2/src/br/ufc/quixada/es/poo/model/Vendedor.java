

package br.ufc.quixada.es.poo.model;

public class Vendedor extends Funcionário {
    private int cargaHorária;



    public void Vendedor() {
    }

    public Vendedor(String nome, String cpf, String matricula, float salario, int cargaHorária) {
        super(nome, cpf, matricula, salario);
        this.cargaHorária = cargaHorária;
    }

    @Override
    public void darBonificacao() {
        setSalario(getSalario() + getSalario() + 3.00f);
    }

    public void realizarVenda(float valorPassagem) {
        setSalario(getSalario() + valorPassagem + 3.00f);
        darBonificacao();
    }

    public int getCargaHorária() {
        return cargaHorária;
    }

    
    public void setCargaHorária(int cargaHorária) {
        this.cargaHorária = cargaHorária;
    }
    
    
      @Override
        public String toString() {
        return "Vendedor  " +cargaHorária + super.toString();
    }

}

