package br.ufc.quixada.es.poo.model;

import java.time.LocalDate;

public class ClientePessoaFísica extends Cliente {

    private String    cpf;
    private LocalDate dataNascimento;

    public void ClientePessoaFísica() {
    }

    public ClientePessoaFísica(String nome, String endereco, String cpf, LocalDate dataNascimento) {
        super(nome, endereco);
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    @Override
    public String toString() {
        return "ClientePessoaFísica cpf = " + cpf +
                ", dataNascimento = " + dataNascimento +
                super.toString();
    }
}