
package br.ufc.quixada.es.poo.model;

import java.util.ArrayList;
import java.util.List;

public abstract class Ônibus implements Imprimivel {

    private int  codÔnibus;
    private String nome;
    private List<Passagem> passagens;
    private List<Cliente>  passageiros;

    public void Ônibus() {
        passagens= new ArrayList<>();
        passageiros = new ArrayList<>();
    }

    public Ônibus(int codÔnibus, String nome, List<Passagem> passagens, List<Cliente> passageiros) {
        this.codÔnibus = codÔnibus;
        this.nome = nome;
        this.passagens = passagens;
        this.passageiros = passageiros;
    }


    public void adicionarPassagemÔnibus(Passagem p) {
        passageiros.add(p.getCliente());
        passagens.add(p);
    }

  
    public void mostraPassagens() {
        for(Passagem p : passagens) {
            System.out.println(p.toString());
        }
    }

    public int getCodÔnibus() {
        return codÔnibus;
    }

    public void setCodÔnibus(int codÔnibus) {
        this.codÔnibus = codÔnibus;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Passagem> getPassagens() {
        return passagens;
    }

    public void setContratos(List<Passagem> passagens) {
        this.passagens = passagens;
    }

    public List<Cliente> getPassageiros() {
        return passageiros;
    }

    public void setPassageiros(List<Cliente> passageiros) {
        this.passageiros = passageiros;
    }

    @Override
    public String toString() {
        return "Ônibus --> codÔnibus = " + codÔnibus +
                ", Nome = " + nome + ", Passagens = " + passagens +
                ", Passageiros = " + passageiros;
    }

    
}


