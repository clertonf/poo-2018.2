package lista.pkg01;

import java.util.Scanner;

public class questão5 {

	public static Scanner calc = new Scanner (System.in);
	public static void main(String[] args) {

		System.out.println("Insira o primeiro numero: ");
		float n1 = calc.nextFloat();
		System.out.println("Insira o segundo numero: ");
		float n2 = calc.nextFloat();
		System.out.println("Observe o menu abaixo e escolha a operao desejada:\n"
				+ "1- Adio\n" + "2- Subtrao\n" + "3- Multiplicao\n" + "4- Diviso\n" + "Insira sua operao desejada:");
		int operacion = calc.nextInt();
		operacao(operacion, n1, n2);
	}
	public static void operacao(int op, float n1, float n2) {
		float result = 0;
		switch(op) {
			case 1:
				result = n1 + n2;
				System.out.println("Soma: "+result);
				break;
			case 2:
				result = n1 - n2;
				System.out.println("Subtração: "+result);
				break;
			case 3:
				result = n1 * n2;
				System.out.println("Multiplicação "+result);
				break;
			case 4:
				if(n2 != 0) {
					result = n1 / n2;
					System.out.println("Resultado: "+result);
					break;
				}else {
					System.out.println("Divisor igual a zero, tente outro numero...");
					break;
				}
			default:
				System.out.println("Operao Invlida");
		}
	}

}
