

package agenda.model;


public class Juridica extends Pessoa {
    private int cnpj;
    private String inscricaoEstadual;
    private String razaoSocial;
        
    public Juridica(String nome, String endereco, String email, int cnpj , String inscricaoEstadual, String razaoSocial) {
        super(nome, endereco, email);
            this.cnpj = cnpj;
            this.inscricaoEstadual = inscricaoEstadual;
            this.razaoSocial = razaoSocial;
            
            
    }

    /**
     * @return the cnpj
     */
    public int getCnpj() {
        return cnpj;
    }

    /**
     * @param cnpj the cnpj to set
     */
    public void setCnpj(int cnpj) {
        this.cnpj = cnpj;
    }

    /**
     * @return the inscricaoEstadual
     */
    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    /**
     * @param inscricaoEstadual the inscricaoEstadual to set
     */
    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    /**
     * @return the razaoSocial
     */
    public String getRazaoSocial() {
        return razaoSocial;
    }

    /**
     * @param razaoSocial the razaoSocial to set
     */
    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }
     
    @Override
    public String toString(){
        return "PessoaJuridica:" + "cnpj:" + cnpj + "Inscricao Estadual:" + inscricaoEstadual+  "Razao Social:" + razaoSocial;
    }
        
    
    
}
