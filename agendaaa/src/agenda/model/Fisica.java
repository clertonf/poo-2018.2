

package agenda.model;
import java.time.LocalDate;

public class Fisica extends Pessoa {
    private int cpf;
    private LocalDate nascimento;
    private String estadocivil;
    
    public  Fisica(String nome, String endereco , String email , int cpf , LocalDate nascimento, String estadocivil){
        super( nome, endereco, email);
           this.cpf = cpf;
           this.nascimento = nascimento;
           this.estadocivil = estadocivil;
        
    }

    /**
     * @return the cpf
     */
    public int getCpf() {
        return cpf;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(int cpf) {
        this.cpf = cpf;
    }

    /**
     * @return the nascimento
     */
    public LocalDate getNascimento() {
        return nascimento;
    }

    /**
     * @param nascimento the nascimento to set
     */
    public void setNascimento(LocalDate nascimento) {
        this.nascimento = nascimento;
    }

    /**
     * @return the estadocivil
     */
    public String getEstadocivil() {
        return estadocivil;
    }

    /**
     * @param estadocivil the estadocivil to set
     */
    public void setEstadocivil(String estadocivil) {
        this.estadocivil = estadocivil;
    }

    public String toString(){
        return "Pessoa Fisica:" + "cpf" + cpf + "nascimento" + nascimento + "estado civil" + estadocivil; 
    }
}
