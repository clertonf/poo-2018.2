package projeto.pkg2.indicar;


public class Motorista {
  
        
        private String nome;
        private String cnh;
        private String placa;
        private float nota;
        
        public Motorista(String nome, String cnh, String placa, float nota){
          
            this.nome = nome;
            this.cnh = cnh;
            this.placa = placa;
            this.nota = nota;
            
        }
      
        public String getNome(){
            return this.nome;
        }
        
        public String getCnh(){
            return this.cnh;
           }
       
        public String getPlaca(){
              return this.placa;
        }
       
        public float getNota(){
              return this.nota;
        }
        
        public void setNome( String nome){
            this.nome = nome;
        }
        
        public void setCnh (String cnh){
            this.cnh = cnh;
        }
        
        public void setPlaca( String placa){
            this.placa = placa;
        }
    
        public void setNota( float nota){
            this.nota = nota;
        }
    
    public void realizarCorrida(Cliente cliente, Corrida corrida){
       
        System.out.print("Nome Cliente:" +cliente.getNome()  );
        System.out.print("Nome Motorista:" +this.nome);
        System.out.print("Nota:" + this.nome);
        System.out.print("Partida:" +corrida.getPartida() );
        System.out.print("Destino:" + corrida.getDestino());
    
    }
    
    @Override
    public String toString (){
        return "\n Nome: " + this.nome + "\n CNH: " + this.cnh + "\n Placa " +this.placa + "\n Nota: " + this.nota;
    }
}


