package projeto.pkg2.indicar;


public class Corrida {
         
        private String partida;
        private String destino;
        private float precoKM;
        private float precoCorrida;
        
        public Corrida(String partida, String destino , float precoKM , float precoCorrida){
            this.partida = partida;
            this.destino = destino;
            this.precoKM = precoKM;
            this.precoCorrida = precoCorrida;
        }

  
    
        public String getPartida(){
            return this.partida;
            }
        public String getDestino(){
            return this.destino;
        }
        public float getPrecoKM(){
             return  this.precoKM;
        }
        public float getPrecoCorrida(){
            return this.precoCorrida;
        }
    
        public void setPartida (String partida){
            this.partida = partida;
        }
        public void setDestino (String destino){
            this.destino = destino;
        }
        public void setPrecoKM (float precoKM){
            this.precoKM = precoKM;
        }
        public void setPrecoCorrida(float precoCorrida){
            this.precoCorrida = precoCorrida;
        }
        
        public float calcularValorCorrida ( int distancia ){
            float n= (this.precoKM * distancia) + 5;
            return n;
         }
    
        @Override
        public String toString(){
            return "\nPartida:" +this.partida+ "\n Destino " + this.destino + "\n precoKM: " + this.precoKM + "\n precoCorrida " + this.precoCorrida;
        }
    }
    
    

